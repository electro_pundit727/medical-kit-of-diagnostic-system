PCD DESIGN For Medical Kit - Diagnostic System

** Goal is to build a PCB for a medical kit that will help those without access to medical care.
PCB will use have a central ATMEGA2560 powered by USB input.

It will use a Hamshield 1.0 - schematic is here 
https://github.com/EnhancedRadioDevices/HamShield1

It will use emic2 - schematic is here
http://www.grandideastudio.com/emic-2-text-to-speech-module/

It will use a ATMEGA 2560 with USB charging into a Lithium Ion battery into the ATMEGA 2560 / Arduino 
https://store.arduino.cc/usa/arduino-mega-2560-rev3